﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClientApp
{
    /// <summary>
    /// Класс для представления таблицы арендатора в базе
    /// </summary>
    [Table("renter")]
    public class Renter
    {
        /// <summary>
        /// id арендатора - является первичным ключем
        /// </summary>
        [Key]
        [Column("id")]
        public long id { get; set; }

        /// <summary>
        /// Паспорт арендатора
        /// </summary>
        [Column("passport")]
        public string passport { get; set; }

        /// <summary>
        /// ФИО арендатора
        /// </summary>
        [Column("fio")]
        public string fio { get; set; }

        /// <summary>
        /// Телефон арендатора
        /// </summary>
        [Column("phone")]
        public string phone { get; set; }

        /// <summary>
        /// id Фотогорафии паспорта арендатора
        /// </summary>
        [Column("id_image")]
        public long id_image { get; set; }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>Строку вида renter {id;ФИО;Телефон;id фото паспорта}</returns>
        public override string ToString()
        {
            return "renter {" + id + "; " + fio + ";" + phone + "; " + id_image + "}";
        }
    }
}
