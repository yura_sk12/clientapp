﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClientApp
{
    /// <summary>
    /// Класс для представления таблицы описания помещения в базе
    /// </summary>
    [Table("gym_description")]
    class Gym_description
    {
        /// <summary>
        /// id описания - является первичным ключем
        /// </summary>
        [Key]
        [Column("id")]
        public long id { get; set; }

        /// <summary>
        /// описание
        /// </summary>
        [Column("description")]
        public string description { get; set; }

        /// <summary>
        /// id оспиваемое помещение
        /// </summary>
        [Column("id_gym")]
        public long id_gym { get; set; }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>строку вида Gym_description {id;описание;id помещения}</returns>
        public override string ToString()
        {
            return "Gym_description {" + id + "; " + description + "; " + id_gym + "}";
        }
    }

}
