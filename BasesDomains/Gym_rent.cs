﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClientApp
{
    /// <summary>
    /// Класс для представления таблицы аренды помещения в базе
    /// </summary>
    [Table("gym_rent")]
    class Gym_rent
    {
        /// <summary>
        /// Дата и время старта аренды
        /// </summary>
        [Column("rent_time")]
        public DateTime rent_time { get; set; }

        /// <summary>
        /// Дата и время окончания аренды
        /// </summary>
        [Column("rent_time_end")]
        public DateTime rent_time_end { get; set; }

        /// <summary>
        /// id помещения - является первичным ключем
        /// </summary>
        [Key, Column("gym_id", Order = 0)]
        public long gym_id { get; set; }

        /// <summary>
        /// id арендатора - является первичным ключем
        /// </summary>
        [Key, Column("renter_id", Order = 1)]
        public long rent_id { get; set; }

        /// <summary>
        /// поле определяющее статус помещения
        /// \param True">Арендован
        /// \param False">Забронирован
        /// </summary>
        [Column("is_rent")]
        public bool is_rent { get; set; }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>Строку вида Gym {Время начала аренды;Время конца аренды;id помещения;id арендатора}</returns>
        public override string ToString()
        {
            return "Gym {" + rent_time + "; " + rent_time_end + "; " + gym_id + "; " + rent_id + "}";
        }
    }
}
