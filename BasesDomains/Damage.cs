﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClientApp
{
    /// <summary>
    /// Класс для представления таблицы повреждений в базе
    /// </summary>
    [Table("damage")]
    class Damage
    {
        /// <summary>
        /// Поле id - является первичным ключем
        /// </summary>
        [Key]
        [Column("id")]
        public long id { get; set; }

        /// <summary>
        /// поле штрафа
        /// </summary>
        [Column("fine")]
        public double fine { get; set; }

        /// <summary>
        /// Описание повреждения
        /// </summary>
        [Column("description")]
        public string description { get; set; }

        /// <summary>
        /// id инвентаря для которого описано повреждение
        /// </summary>
        [Column("stock_id")]
        public long stock_id { get; set; }

        /// <summary>
        /// id арендатора которых повредил инвентарь
        /// </summary>
        [Column("renter_id")]
        public long renter_id { get; set; }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>Строка вида damage {id;штраф;описание;id инвентаря;id арендатора}</returns>
        public override string ToString()
        {
            return "damage {" + id + "; " + fine + ";" + description + "; " + stock_id + "; " +  renter_id + "}";
        }
    }
}
