﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClientApp
{
    /// <summary>
    /// Класс для представления таблицы инвентаря в базе
    /// </summary>
    [Table("stock")]
    class Stock
    {
        /// <summary>
        /// id инвентаря - является первичным ключем
        /// </summary>
        [Key]
        [Column("id")]
        public long id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Column("name")]
        public string name { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        [Column("cost")]
        public double cost { get; set; }

        /// <summary>
        /// точка выдачи
        /// </summary>
        [Column("point_to_rent")]
        public string point_to_rent { get; set; }

        /// <summary>
        /// id фото инвентаря
        /// </summary>
        [Column("id_image")]
        public long id_image { get; set; }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>Строка вида stock {id;наименование;стоимость;точка выдачи;id фото}</returns>
        public override string ToString()
        {
            return "stock {" + id + "; " + name + ";" + cost + "; " + point_to_rent + "; " + id_image + "}";
        }

    }
}
