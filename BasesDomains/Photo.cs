﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClientApp
{
    /// <summary>
    /// Класс для представления таблицы фото в базе
    /// </summary>
    [Table("photo")]
    class Photo
    {
        /// <summary>
        /// id фото - является первичным ключем
        /// </summary>
        [Key]
        [Column("id")]
        public long id { get; set; }

        /// <summary>
        /// Путь к фото
        /// </summary>
        [Column("path")]
        public string path { get; set; }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>Строку вида Photos {id;путь}</returns>
        public override string ToString()
        {
            return "Photos {" + id + "; " + path + "}";
        }
    }
}
