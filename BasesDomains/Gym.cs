﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClientApp
{
    /// <summary>
    /// Класс для представления таблицы помещения в базе
    /// </summary>
    [Table("gym")]
    class Gym
    {
        /// <summary>
        /// id помещения - является первичным ключем
        /// </summary>
        [Key]
        [Column("id")]
        public long id { get; set; }

        /// <summary>
        /// адрес помещения
        /// </summary>
        [Column("address")]
        public string address { get; set; }

        /// <summary>
        /// стоимость аренды помещения
        /// </summary>
        [Column("cost")]
        public double cost { get; set; }

        /// <summary>
        /// id используемого изображения
        /// </summary>
        [Column("id_image")]
        public long id_image { get; set; }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>Строку вида Gym {id;адрем;стоимость;id картинки}</returns>
        public override string ToString()
        {
            return "Gym {" + id + "; " + address + "; " + cost + "; " + id_image + "}";
        }
    }
}
