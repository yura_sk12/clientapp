﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClientApp
{
    /// <summary>
    /// Класс для представления таблицы ареды инвентаря в базе
    /// </summary>
    [Table("stock_rent")]
    class StockRent
    {
        /// <summary>
        /// Дата и время старта аренды
        /// </summary>
        [Column("rent_time")]
        public DateTime rent_time { get; set; }

        /// <summary>
        /// Дата и время окончания аренды
        /// </summary>
        [Column("rent_time_end")]
        public DateTime rent_time_end { get; set; }

        /// <summary>
        /// id инвентаря - является первичным ключем
        /// </summary>
        [Key, Column("stock_id", Order = 1)]
        public long stock_id { get; set; }

        /// <summary>
        /// id арендатора - является первичным ключем
        /// </summary>
        [Key, Column("renter_id", Order = 2)]
        public long renter_id { get; set; }

        /// <summary>
        /// поле определяющее статус инвентаря
        /// \param True">Арендован
        /// \param False">Забронирован
        /// </summary>
        [Column("is_rent")]
        public bool is_rent { get; set; }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>Строку вида Gym {Время начала аренды;Время конца аренды;id инвентаря;id арендатора}</returns>
        public override string ToString()
        {
            return "stock_rent {" + rent_time + "; " + rent_time_end + ";" + stock_id + "; " + renter_id + "; " + is_rent + "}";
        }
    }
}
