﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ClientApp
{
    /// <summary>
    /// Форма добавления арентатора
    /// </summary>
    public partial class Add_renter_form : Form
    {
        /// <summary>
        /// Конструктор - инициализирует только компоненты окна
        /// </summary>
        public Add_renter_form()
        {
            InitializeComponent();
        }

        /// <summary>
        /// путь к фото паспорта
        /// </summary>
        public string renter_img_path
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        /// <summary>
        /// ФИО арендатора
        /// </summary>
        public string renter_fio
        {
            get
            {
                return textBox2.Text;
            }
            set
            {
                textBox2.Text = value;
            }
        }

        /// <summary>
        /// Паспорт арендатора
        /// </summary>
        public string renter_passport
        {
            get
            {
                return textBox3.Text;
            }
            set
            {
                textBox3.Text = value;
            }
        }

        /// <summary>
        /// Телефон арентатора
        /// </summary>
        public string renter_phone
        {
            get
            {
                return textBox4.Text;
            }
            set
            {
                textBox4.Text = value;
            }
        }

        /// <summary>
        /// Выбор фото
        /// </summary>
        /// <param name="sender">Button выбора фото</param>
        /// <param name="e">Класс событий</param>
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files(*.jpeg)|*.jpeg|" +
                "Image files(*.jpg)|*.jpg|" +
                "Image files(*.png)|*.png|" +
                "All files(*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = openFileDialog.FileName;
            textBox1.Text = filename.ToString();
        }

        /// <summary>
        /// Кнопка Ок
        /// </summary>
        /// <param name="sender">Button Ок</param>
        /// <param name="e">Класс событий</param>
        private void button5_Click(object sender, EventArgs e)
        {
            // Проверка пасспорта
            bool res = check_passport(textBox3.Text);
            if (!res)
            {
                MessageBox.Show("Проверте правильность ввода паспорта");
                return;
            }

            // проверка номера телефона
            res = check_phone(textBox4.Text);
            if (!res)
            {
                MessageBox.Show("Проверте правильность ввода телефона");
                return;
            }

            // проверка фамилии
            res = check_fio(textBox2.Text);
            if (!res)
            {
                MessageBox.Show("Проверте правильность ввода фамилии\r\nПример Иванов Иван Иванович");
                return;
            }

            // проверка пути с картинке
            res = check_path(textBox1.Text);
            if (!res)
            {
                MessageBox.Show("Проверте правильность ввода пути");
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Кнопка отмена
        /// </summary>
        /// <param name="sender">Button отмена</param>
        /// <param name="e">Класс событий</param>
        private void button4_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Проверка пути
        /// </summary>
        /// <param name="str">Путь с изображению</param>
        /// <returns>false - Строка не правильная true - строка верная</returns>
        /// \warning На данном этапе проверяется только на пустую строку, если пустая false иначе true
        private bool check_path(string str)
        {
            if (str == "")
            {
                return false;
            } else
            {
                return true;
            }
        }

        /// <summary>
        /// Проваерка пасспорта по регулярному выражению
        /// </summary>
        /// <param name="str">Номер паспорта</param>
        /// <returns>Результат сравнения с паттерном</returns>
        private bool check_passport(string str)
        {
            string pattern = @"\d{4}\s\d{6}$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(str);
        }

        /// <summary>
        /// Проверка фамилии по регулярному выражению
        /// </summary>
        /// <param name="str">Фамилия</param>
        /// <returns>Результат сравнения с паттерном</returns>
        private bool check_fio(string str)
        {
            string pattern = @"([А-ЯЁ][а-яё]+[\-\s]?){3,}$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(str);
        }


        /// <summary>
        /// Проверка телефона по регулярному выражению
        /// </summary>
        /// <param name="str">Номер телефона</param>
        /// <returns>Результат сравнения с паттерном</returns>
        private bool check_phone(string str)
        {
            string pattern = @"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(str);
        }

        /// <summary>
        /// событие на изменение пасспорта
        /// </summary>
        /// <param name="sender">TextBox номера паспорта</param>
        /// <param name="e">Класс событий</param>
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            bool res = check_passport(((TextBox)sender).Text);
            if (!res)
            {
                textBox3.BackColor = Color.Pink;
            } else
            {
                textBox3.BackColor = Color.White;
            }
        }

        /// <summary>
        /// Событие на изменение номера телефона
        /// </summary>
        /// <param name="sender">TextBox номера телефона</param>
        /// <param name="e">Класс событий</param>
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            bool res = check_phone(((TextBox)sender).Text);
            if (!res)
            {
                textBox4.BackColor = Color.Pink;
            }
            else
            {
                textBox4.BackColor = Color.White;
            }
        }

        /// <summary>
        /// Событие на изменение ФИО 
        /// </summary>
        /// <param name="sender">TextBox ФИО</param>
        /// <param name="e">Класс событий</param>
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            bool res = check_fio(((TextBox)sender).Text);
            if (!res)
            {
                textBox2.BackColor = Color.Pink;
            }
            else
            {
                textBox2.BackColor = Color.White;
            }
        }
    }
}
