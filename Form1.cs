﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    /// <summary>
    /// Класс формы главного окна приложения
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// Конструктор класса
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            load_all_stock();
            LoadDataToRoomListView();
        }

        /// <summary>
        /// Метод загруки инвентаря из базы
        /// </summary>
        private void load_all_stock()
        {
            var context = new StockRepos();
            var stocks = context.getAllStock();
            listView1.Items.Clear();
            foreach (var stock in stocks)
            {
                int status = get_stock_status(stock);
                string status_str = "";
                switch (status)
                {
                    case 1: //Арендован
                        status_str = "Арендован";
                        break;
                    case 2: // Забронирован
                        status_str = "Заброниорван";
                        break;
                    case 0: // Свободен
                        status_str = "Свободен";
                        break;
                    default:
                        status_str = "Неизвестно";
                        break;
                }

                string[] row = {
                    Convert.ToString(stock.id),
                    stock.name,
                    Convert.ToString(stock.cost),
                    stock.point_to_rent,
                    status_str
                };
                listView1.Items.Add(new ListViewItem(row));
            }
        }

        /// <summary>
        /// Метод очисти полей пасспорта инвентаря
        /// </summary>
        public void clear_stock_passport()
        {
            pictureBox1.Image = null;
            listView2.Items.Clear();
            label2.Text = "";
            label4.Text = "";
            label5.Text = "";
            label11.Text = "";
        }

        /// <summary>
        /// Метод очисти полей пасспорта помещения
        /// </summary>
        public void clear_gym_passport()
        {
            pictureBox2.Image = null;
            listView3.Items.Clear();
            label9.Text = "";
            label7.Text = "";
            label6.Text = "";
            label12.Text = "";
        }

        /// <summary>
        /// Метод загрузки помещений из базы данных
        /// </summary>
        public void LoadDataToRoomListView()
        {
            GymRepos gymRepos = new GymRepos();
            List<Gym> gyms = gymRepos.getAll().ToList();

            listView4.Items.Clear();
            foreach (Gym gym in gyms)
            {
                Gym_rent gym_rent = get_gym_status(gym);
                string status_str;
                if (gym_rent == null)
                {
                    status_str = "Свободен";
                }
                else
                {
                    if (gym_rent.is_rent)
                    {
                        status_str = "Арендован";
                    }
                    else
                    {
                        status_str = "Забронирован";
                    }
                }
                string[] row = { Convert.ToString(gym.id), gym.address.ToString(),
                    Convert.ToString(gym.cost), status_str };
                listView4.Items.Add(new ListViewItem(row));
            }
        }

        /// <summary>
        /// Получение статуса помещения
        /// </summary>
        /// <param name="gym_in">Идентификатор помещения</param>
        /// <returns>Объект помещения</returns>
        private Gym_rent get_gym_status(Gym gym_in)
        {
            Gym_rentRepos gymRentRepos = new Gym_rentRepos();
            List<Gym_rent> gymRents = gymRentRepos.getByIdGym(gym_in.id).ToList();

            if (gymRents.Count > 0)
            {
                return gymRents[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Определяет статус инвентаря из базы
        /// </summary>
        /// <param name="stock">Объект инвентаря</param>
        /// <returns>1-Арендован, 0-свободен, 2-забронирован</returns>
        private int get_stock_status(Stock stock)
        {
            var stockrent_context = new StockRentRepos();
            var stockrents = stockrent_context.getByIdStock(stock.id).ToList();

            if (stockrents.Count > 0)
            {
                if (stockrents[0].is_rent)
                {
                    // Арендован
                    return 1;
                }
                else
                {
                    // Забронирован
                    return 2;
                }
            }
            else
            {
                // Свободен
                return 0;
            }
        }

        /// <summary>
        /// Обработка кнопки редатирования инвентаря
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Edit_Invent_Form editinvent = new Edit_Invent_Form();
            editinvent.Text = "Редактирование инвентаря";
            editinvent.listView2.Enabled = true;
            int result_code = editinvent.get_result_code();
            ListView.SelectedListViewItemCollection selected_item = listView1.SelectedItems;
            clear_stock_passport();
            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item == null)
            {
                return;
            } else  // Если ничего не выделено
            {
                // Ищем Stock по выделенному ID
                StockRepos stockRepos = new StockRepos();
                Stock stock = stockRepos.getById(Convert.ToInt64(selected_item[0].Text));
                if (stock != null)
                {
                    // Ищем картинку для Stock
                    var photoRepos = new PhotoRepos();
                    Photo photo = photoRepos.getById(stock.id_image);

                    // Поиск поломок
                    var damageRepos = new DamageRepos();
                    List<Damage> damages = damageRepos.getByStockID(stock.id).ToList();

                    // Поиск ренторов
                    RenterRepos renterRepos = new RenterRepos();
                    Renter renter = new Renter();

                    // Заполнение полей
                    editinvent.invent_cost = stock.cost.ToString();
                    editinvent.invent_img_path = photo.path;
                    editinvent.invent_name = stock.name;
                    editinvent.invent_point_to_rent = stock.point_to_rent;
                    editinvent.invent_id = stock.id;

                    // Заполнение списка повреждений
                    editinvent.listView2.Items.Clear();
                    foreach (var damage in damages)
                    {
                        // Поиск виновника
                        renter = renterRepos.getById(damage.renter_id);

                        string[] row = { damage.description, renter.fio, Convert.ToString(damage.fine), renter.passport.ToString(), renter.id.ToString()};
                        editinvent.listView2.Items.Add(new ListViewItem(row));
                    }

                    // Обработка диалога
                    editinvent.ShowDialog();
                    if (editinvent.DialogResult != DialogResult.OK)
                    {
                        return;
                    }

                    // сохранение измененных данных
                    photo.path = editinvent.invent_img_path;
                    stock.name = editinvent.invent_name;
                    stock.point_to_rent = editinvent.invent_point_to_rent;
                    stock.cost = Convert.ToDouble(editinvent.invent_cost);

                    // Удаление списка дефектов
                    foreach (Damage damage in damages)
                    {
                        damageRepos.Delete(damage.id);
                    }
                    damageRepos.Save();

                    // Добавление списка дефектов
                    ListView.ListViewItemCollection new_damages = editinvent.listView2.Items;
                    foreach (ListViewItem item in new_damages)
                    {
                        Damage damage = new Damage();
                        damage.renter_id = new RenterRepos().getById(Convert.ToInt64(item.SubItems[4].Text)).id;
                        damage.stock_id = stock.id;
                        damage.description = item.SubItems[0].Text;
                        damage.fine = Convert.ToDouble(item.SubItems[2].Text);
                        damageRepos.Create(damage);
                    }

                    // Запись данных в БД
                    damageRepos.Save();
                    stockRepos.Update(stock);
                    stockRepos.Save();
                    photoRepos.Update(photo);
                    photoRepos.Save();

                    // Обновление списка
                    load_all_stock();
                    clear_stock_passport();
                }
            }
        }

        /// <summary>
        /// Устанока статуса для инвентаря
        /// </summary>
        /// <param name="is_rent">Статус (1 - Аренда)</param>
        private void set_status_stock(bool is_rent)
        {
            Arenda_Form arendaform = new Arenda_Form();
            ListView.SelectedListViewItemCollection selected_item = listView1.SelectedItems;
            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item == null)
            {
                return;
            }

            // Получение объекта инвентаря
            StockRepos stockRepos = new StockRepos();
            Stock stock = stockRepos.getById(Convert.ToInt64(selected_item[0].Text));

            // Заполнение формы
            if (is_rent) arendaform.Text = "Аренда инвентаря";
            else arendaform.Text = "Бронирование инвентаря";
            arendaform.article = stock.id.ToString();
            arendaform.name = stock.name;

            // DialogResult
            arendaform.ShowDialog();
            if (arendaform.DialogResult != DialogResult.OK)
            {
                return;
            }

            // Получение данных с формы
            Renter renter = arendaform.renter;
            DateTime date_start = arendaform.date_start;
            DateTime date_end = arendaform.date_end;

            // Запись данных в базу
            StockRentRepos stockRentRepos = new StockRentRepos();
            StockRent stockRent = new StockRent();
            stockRent.is_rent = is_rent;
            stockRent.renter_id = renter.id;
            stockRent.stock_id = stock.id;
            stockRent.rent_time = date_start;
            stockRent.rent_time_end = date_end;
            stockRentRepos.Create(stockRent);
            stockRentRepos.Save();
            load_all_stock();
        }
        
        /// <summary>
        /// Обработка кнопки аренды инвентаря
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            bool res = check_for_rent_stock();
            if (res)
                set_status_stock(true);
        }

        /// <summary>
        /// Проверка статуса инвентаря
        /// </summary>
        /// <returns>Статус (true - свободен, false - занят)</returns>
        private bool check_for_rent_stock()
        {
            Arenda_Form arendaform = new Arenda_Form();
            ListView.SelectedListViewItemCollection selected_item = listView1.SelectedItems;
            if (selected_item.Count == 0)
            {
                return false;
            }
            if (selected_item == null)
            {
                return false;
            }

            // Получение объекта инвентаря
            StockRepos stockRepos = new StockRepos();
            Stock stock = stockRepos.getById(Convert.ToInt64(selected_item[0].Text));
            int status = get_stock_status(stock);
            if (status != 0)
            {
                if (status == 1)
                {
                    MessageBox.Show("Инвентарь уже арендован");
                    return false;
                }
                if (status == 2)
                {
                    MessageBox.Show("Инвентарь уже забронирован");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Обработка кнопки Бронирование инвентаря
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            bool res = check_for_rent_stock();
            if (res)
                set_status_stock(false);
        }

        /// <summary>
        /// Обработка кнопки Освобождение инвентаря
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection selected_item = listView1.SelectedItems;
            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item == null)
            {
                return;
            }

            // Получение объекта инвентаря
            StockRepos stockRepos = new StockRepos();
            Stock stock = stockRepos.getById(Convert.ToInt64(selected_item[0].Text));

            // Удаление арендатора
            StockRentRepos stockRentRepos = new StockRentRepos();
            List<StockRent> stockRents = stockRentRepos.getByIdStock(stock.id).ToList();
            foreach (StockRent item in stockRents)
            {
                stockRentRepos.Delete(item);
            }
            stockRentRepos.Save();

            // Перезагрузка дданых на форме
            clear_stock_passport();
            load_all_stock();
        }

        /// <summary>
        /// Установка статуса для помещения
        /// </summary>
        /// <param name="is_rent">Статус (true - аренда, false - бронь)</param>
        private void set_status_gym(bool is_rent)
        {
            Arenda_Form arendaform = new Arenda_Form();
            ListView.SelectedListViewItemCollection selected_item = listView4.SelectedItems;
            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item == null)
            {
                return;
            }

            // Получение объекта помещения
            GymRepos gymRepos = new GymRepos();
            Gym gym = gymRepos.getById(Convert.ToInt64(selected_item[0].Text));

            // Заполнение формы
            if (is_rent) arendaform.Text = "Аренда помещения";
            else arendaform.Text = "Бронирование помещения";

            arendaform.article = gym.id.ToString();
            arendaform.name = gym.address;

            // DialogResult
            arendaform.ShowDialog();
            if (arendaform.DialogResult != DialogResult.OK)
            {
                return;
            }

            // Получение данных с формы
            Renter renter = arendaform.renter;
            DateTime date_start = arendaform.date_start;
            DateTime date_end = arendaform.date_end;

            // Запись данных в базу
            Gym_rentRepos gymRentRepos = new Gym_rentRepos();
            Gym_rent gym_rent = new Gym_rent();
            gym_rent.is_rent = is_rent;
            gym_rent.rent_id = renter.id;
            gym_rent.gym_id = gym.id;
            gym_rent.rent_time = date_start;
            gym_rent.rent_time_end = date_end;
            gymRentRepos.Create(gym_rent);
            gymRentRepos.Save();
            LoadDataToRoomListView();
        }

        /// <summary>
        /// Обработка кнопки Аренда помещения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button11_Click(object sender, EventArgs e)
        {
            bool res = check_for_rent_gym();
            if (res)
                set_status_gym(true);
        }

        /// <summary>
        /// Обработка кнопки Бронирование помещения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button10_Click(object sender, EventArgs e)
        {
            bool res = check_for_rent_gym();
            if (res)
                set_status_gym(false);
        }

        /// <summary>
        /// Проверка статуса помещения
        /// </summary>
        /// <returns>Статус (true - свободен, false - занят)</returns>
        private bool check_for_rent_gym()
        {
            Arenda_Form arendaform = new Arenda_Form();
            ListView.SelectedListViewItemCollection selected_item = listView4.SelectedItems;
            if (selected_item.Count == 0)
            {
                return false;
            }
            if (selected_item == null)
            {
                return false;
            }

            // Получение объекта помещения
            GymRepos gymRepos = new GymRepos();
            Gym gym = gymRepos.getById(Convert.ToInt64(selected_item[0].Text));

            Gym_rent gym_rent = get_gym_status(gym);
            if (gym_rent != null)
            {
                RenterRepos renterRepos = new RenterRepos();
                Renter rent = renterRepos.getById(gym_rent.rent_id);
                if (gym_rent.is_rent)
                {
                    MessageBox.Show("Инвентарь уже арендован");
                    return false;
                }
                else
                {
                    MessageBox.Show("Инвентарь уже забронирован");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Обработка кнопки Изменение помещения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button13_Click(object sender, EventArgs e)
        {
            Edit_Room_Form roomform = new Edit_Room_Form();
            roomform.Text = "Редактирование помещения";
            GymRepos context = new GymRepos();
            Gym gym = null;

            PhotoRepos photo_context = new PhotoRepos();
            Photo photo = null;

            Gym_descRepos gym_desc_context = new Gym_descRepos();
            List<Gym_description> gym_descriptions = null;

            // Получаем выбранный gym
            ListView.SelectedListViewItemCollection selected_item = listView4.SelectedItems;

            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item != null)
            {
                // Ищем Gym по выделенному ID
                gym = context.getById(Convert.ToInt64(selected_item[0].Text));
                if (gym != null)
                { // есть такой Gym
                    // Ищем картинку для Gym
                    photo = photo_context.getById(gym.id_image);

                    // Поиск описаний для Gym
                    gym_descriptions = gym_desc_context.getByGymID(gym.id).ToList();

                    // Есть все данные для заполнения формы !!!
                    // Заполняем форму данными фото помещения
                    roomform.room_id = Convert.ToString(gym.id);
                    roomform.room_img_path = photo.path;

                    // Заполняем форму данными для помещения
                    roomform.address = gym.address.ToString();
                    roomform.cost = Convert.ToString(gym.cost);

                    foreach (var gym_description in gym_descriptions)
                    {
                        // Добавление в ListView для формы

                        string[] row = { gym_description.description };
                        roomform.listViewItems.Add(new ListViewItem(row));
                    }

                }
                else
                { // Нет такого Gym
                    return;
                }

                // Форма заполнена
                // Теперь запускаем форму
                roomform.ShowDialog();
                if (roomform.DialogResult == DialogResult.OK)
                { // Форма закыта нажатием Ок
                    // Обновление фото
                    photo.path = roomform.room_img_path;
                    photo_context.Update(photo);
                    photo_context.Save();

                    // Обновление помещения
                    gym.address = roomform.address;
                    gym.cost = Convert.ToDouble(roomform.cost);
                    context.Update(gym);
                    context.Save();

                    // Обновление описаний помещения
                    // Сначала удалим старые
                    foreach (var gym_description in gym_descriptions)
                    {
                        gym_desc_context.Delete(gym_description.id);
                        gym_desc_context.Save();
                    }
                    // Добавим новые
                    // Добавление описаний для помещения
                    Gym_description gym_desc = null;
                    foreach (ListViewItem eachItem in roomform.listViewItems)
                    {
                        gym_desc = new Gym_description();
                        gym_desc.description = eachItem.Text.ToString();
                        gym_desc.id_gym = gym.id;

                        gym_desc_context.Create(gym_desc);
                        gym_desc_context.Save();
                    }
                    LoadDataToRoomListView();
                }
                else if (roomform.DialogResult == DialogResult.Cancel)
                { // Нажата отмена
                    return;
                }
            }
        }

        /// <summary>
        /// Обработка кнопки Освобождение помещения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button9_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection selected_item = listView4.SelectedItems;
            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item == null)
            {
                return;
            }

            // Получение объекта инвентаря
            GymRepos gymRepos = new GymRepos();
            Gym gym = gymRepos.getById(Convert.ToInt64(selected_item[0].Text));

            // Удаление арендатора
            Gym_rentRepos gym_rentRepos = new Gym_rentRepos();
            List<Gym_rent> gymRents = gym_rentRepos.getByIdGym(gym.id).ToList();
            foreach (Gym_rent item in gymRents)
            {
                gym_rentRepos.Delete(item);
            }
            gym_rentRepos.Save();

            clear_gym_passport();
            LoadDataToRoomListView();
        }

        /// <summary>
        /// Обработка кнопки Добавление помещения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button14_Click(object sender, EventArgs e)
        {
            Edit_Room_Form roomform = new Edit_Room_Form();
            roomform.Text = "Добавление помещения";
            roomform.ShowDialog();
            if (roomform.DialogResult == DialogResult.OK)
            {  // Нажата кнопка Ок
                PhotoRepos photoRepos = new PhotoRepos();
                GymRepos gymRepos = new GymRepos();
                Gym_descRepos gym_descRepos = new Gym_descRepos();
                Photo photo = new Photo();
                Gym gym = new Gym();
                Gym_description gym_description;

                // Добавление фото
                photo.path = roomform.room_img_path;
                photoRepos.Create(photo);
                photoRepos.Save();
                long id_image = photo.id; // Здесь id добавленого фото

                // Добавление помещения
                gym.address = roomform.address;
                gym.cost = Convert.ToDouble(roomform.cost);
                gym.id_image = id_image;
                gymRepos.Create(gym);
                gymRepos.Save();
                long id_gym = gym.id; // Здесь id добавленного помещения

                // Добавление описаний для помещения
                foreach (ListViewItem eachItem in roomform.listViewItems)
                {
                    gym_description = new Gym_description();
                    gym_description.description = eachItem.Text.ToString();
                    gym_description.id_gym = id_gym;

                    gym_descRepos.Create(gym_description);
                    gym_descRepos.Save();
                }
                LoadDataToRoomListView();
            }
            else if (roomform.DialogResult == DialogResult.Cancel)
            {  // Нажата отмена
                return;
            }
        }
        
        /// <summary>
        /// Обработка кнопки Добавление инвентаря
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Edit_Invent_Form inventform = new Edit_Invent_Form();
            inventform.Text = "Добавление инвентаря";
            inventform.ShowDialog();
            int result_code = inventform.get_result_code();
            if (result_code == 0)
            {  // Нажата кнопка Ок
                PhotoRepos photoRepos = new PhotoRepos();
                StockRepos stockRep = new StockRepos();
                Photo photo = new Photo();
                Stock stock = new Stock();

                // Добавляет photo
                photo.path = inventform.invent_img_path;
                photoRepos.Create(photo);
                photoRepos.Save();
                long id_img = photo.id;

                // Дабовляем инвентарь
                stock.id_image = id_img;
                stock.name = inventform.invent_name;
                stock.point_to_rent = inventform.invent_point_to_rent;
                stock.cost = Convert.ToDouble(inventform.invent_cost);
                stockRep.Create(stock);
                stockRep.Save();
                
                // Поиск поломок
                var damageRepos = new DamageRepos();
                List<Damage> damages = damageRepos.getByStockID(stock.id).ToList();

                // Удаление списка дефектов
                foreach (Damage damage in damages)
                {
                    damageRepos.Delete(damage.id);
                }
                damageRepos.Save();

                // Добавление списка дефектов
                ListView.ListViewItemCollection new_damages = inventform.listView2.Items;
                foreach (ListViewItem item in new_damages)
                {
                    Damage damage = new Damage();
                    damage.renter_id = new RenterRepos().getById(Convert.ToInt64(item.SubItems[4].Text)).id;
                    damage.stock_id = stock.id;
                    damage.description = item.SubItems[0].Text;
                    damage.fine = Convert.ToDouble(item.SubItems[2].Text);
                    damageRepos.Create(damage);
                }

                // Запись данных в БД
                damageRepos.Save();

                // Загрузка всего инвентаря
                load_all_stock();
            }
            else if (result_code == 1)
            {  // Нажата отмена
                return;
            }
        }

        /// <summary>
        /// Обработка выделения строки из списка инвентаря
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView listView = (ListView)sender;
            ListView.SelectedListViewItemCollection selected_item = listView.SelectedItems;
            clear_stock_passport();
            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item != null)
            {
                // Ищем Stock по выделенному ID
                var context = new StockRepos();
                Stock stock = context.getById(Convert.ToInt64(selected_item[0].Text));
                if (stock != null)
                {
                    // Ищем картинку для Stock
                    var photo_context = new PhotoRepos();
                    Photo photo = photo_context.getById(stock.id_image);

                    // Поиск поломок
                    var damage_context = new DamageRepos();
                    var damages = damage_context.getByStockID(stock.id).ToList();

                    listView2.Items.Clear();
                    foreach (var damage in damages)
                    {
                        // Поиск виновника
                        var renter_context = new RenterRepos();
                        var renter = renter_context.getById(damage.renter_id);

                        string[] row = { damage.description, renter.fio, Convert.ToString(damage.fine) };
                        listView2.Items.Add(new ListViewItem(row));
                    }

                    try
                    {
                        pictureBox1.Image = new Bitmap(photo.path);
                    }
                    catch
                    {

                    }

                    //Стоимость
                    label2.Text = stock.cost.ToString();
                    // Пункт выдачи
                    label4.Text = stock.point_to_rent;

                    // Статус - свободен, арендован, забронирован
                    // Для начала ищем в StockRent время аред, если есть смотрим статус, иначе "свободен
                    int status = get_stock_status(stock);

                    switch (status)
                    {
                        case 1:
                            {
                                // Арендован
                                var stockrent_context = new StockRentRepos();
                                List<StockRent> stockrents = stockrent_context.getByIdStock(stock.id).ToList();

                                RenterRepos renterRepos = new RenterRepos();
                                Renter rent = renterRepos.getById(stockrents[0].renter_id);

                                label5.Text = "Арендован: " + rent.fio;
                                label5.BackColor = Color.Red;
                                label11.Text = "до " + stockrents[0].rent_time_end.ToShortDateString();
                                label11.BackColor = Color.Red;
                                break;
                            }
                        case 2:
                            {
                                // Забронирован
                                var stockrent_context = new StockRentRepos();
                                List<StockRent> stockrents = stockrent_context.getByIdStock(stock.id).ToList();

                                RenterRepos renterRepos = new RenterRepos();
                                Renter rent = renterRepos.getById(stockrents[0].renter_id);

                                label5.Text = "Заброниорван: " + rent.fio;
                                label5.BackColor = Color.Red;
                                label11.Text = "до " + stockrents[0].rent_time_end.ToShortDateString();
                                label11.BackColor = Color.Red;
                                break;
                            }
                        case 0:
                            {
                                // Свободен
                                label5.Text = "Свободен";
                                label5.BackColor = Color.Green;
                                break;
                            }
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Обработка выделения строки из списка помещений
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView4_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView listView = (ListView)sender;
            ListView.SelectedListViewItemCollection selected_item = listView.SelectedItems;
            clear_gym_passport();
            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item != null)
            {
                // Ищем Gym по выделенному ID
                GymRepos context = new GymRepos();
                Gym gym = context.getById(Convert.ToInt64(selected_item[0].Text));
                if (gym != null)
                {
                    // Ищем картинку для Gym
                    PhotoRepos photo_context = new PhotoRepos();
                    Photo photo = photo_context.getById(gym.id_image);

                    // Поиск описаний для Gym
                    Gym_descRepos gym_desc_context = new Gym_descRepos();
                    List<Gym_description> gym_descriptions = gym_desc_context.getByGymID(gym.id).ToList();

                    listView3.Items.Clear();

                    foreach (var gym_description in gym_descriptions)
                    {
                        // Добавление в ListView

                        string[] row = { gym_description.description };
                        listView3.Items.Add(new ListViewItem(row));
                    }

                    // Выводим картинку
                    try
                    {
                        pictureBox2.Image = new Bitmap(photo.path);
                    }
                    catch
                    {
                    }

                    //Стоимость
                    label9.Text = Convert.ToString(gym.cost);
                    // Адрес
                    label7.Text = gym.address.ToString();

                    // Статус - свободен, арендован, забронирован
                    // Для начала ищем в GymRent время аред, если есть смотрим статус, иначе "свободен
                    Gym_rent gym_rent = get_gym_status(gym);
                    if (gym_rent == null)
                    {
                        // Свободен
                        label6.Text = "Свободен";
                        label12.BackColor = Color.Transparent;
                        label6.BackColor = Color.Green;
                    }
                    else
                    {
                        RenterRepos renterRepos = new RenterRepos();
                        Renter rent = renterRepos.getById(gym_rent.rent_id);
                        if (gym_rent.is_rent)
                        {
                            label6.Text = "Арендован " + rent.fio;
                            label12.Text = "до " + gym_rent.rent_time_end.ToShortDateString();
                            label12.BackColor = Color.Pink;
                            label6.BackColor = Color.Pink;
                        }
                        else
                        {
                            label6.Text = "Забронирован " + rent.fio;
                            label12.Text = "до " + gym_rent.rent_time_end.ToShortDateString();
                            label12.BackColor = Color.Pink;
                            label6.BackColor = Color.Pink;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Обработка кнопки Удаление помещения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button12_Click(object sender, EventArgs e)
        {
            Gym_rentRepos gymRentRepos = new Gym_rentRepos();
            List<Gym_rent> gymRents = null;

            GymRepos context = new GymRepos();
            Gym gym = null;

            PhotoRepos photo_context = new PhotoRepos();
            Photo photo = null;

            Gym_descRepos gym_desc_context = new Gym_descRepos();
            List<Gym_description> gym_descriptions = null;

            // Получаем выбранный gym
            ListView.SelectedListViewItemCollection selected_item = listView4.SelectedItems;

            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item != null)
            {
                // Ищем Gym по выделенному ID
                gym = context.getById(Convert.ToInt64(selected_item[0].Text));
                if (gym != null)
                { // есть такой Gym

                    // Проверяем свободно ли помещение
                    gymRents = gymRentRepos.getByIdGym(gym.id).ToList();
                    if (gymRents.Count > 0)
                    { // Помещение не свободно
                        if (MessageBox.Show("Помещение в аренде! Удалить?",
                            "Внимание!",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning,
                            MessageBoxDefaultButton.Button1,
                            MessageBoxOptions.DefaultDesktopOnly) == DialogResult.Yes)
                        { // Если пришло подтверждение на удаление
                            // Удаление аренды
                            foreach (Gym_rent gymRent in gymRents)
                            {
                                gymRentRepos.Delete(gymRent);
                                gymRentRepos.Save();
                            }
                        }
                        else
                        {
                            return;
                        }
                        
                    }

                    // Ищем картинку для Gym
                    photo = photo_context.getById(gym.id_image);

                    // Поиск описаний для Gym
                    gym_descriptions = gym_desc_context.getByGymID(gym.id).ToList();

                    // Удаляем описания помещения
                    foreach (var gym_description in gym_descriptions)
                    {
                        gym_desc_context.Delete(gym_description.id);
                        gym_desc_context.Save();
                    }

                    //Удаляем помещение
                    context.Delete(gym.id);
                    context.Save();
                    // Удаляем фото
                    photo_context.Delete(photo.id);
                    photo_context.Save();
                }
                clear_gym_passport();
                LoadDataToRoomListView();
            }
        }

        /// <summary>
        /// Обработка кнопки Удаление инвентаря
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection selected_item = listView1.SelectedItems;
            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item == null)
            {
                return;
            }

            // Получение объекта инвентаря
            StockRepos stockRepos = new StockRepos();
            Stock stock = stockRepos.getById(Convert.ToInt64(selected_item[0].Text));

            // Удаление арендатора
            StockRentRepos stockRentRepos = new StockRentRepos();
            List<StockRent> stockRents = stockRentRepos.getByIdStock(stock.id).ToList();
            foreach (StockRent item in stockRents)
            {
                stockRentRepos.Delete(item);
            }
            stockRentRepos.Save();

            // Ищем картинку для Stock
            PhotoRepos photoRepos = new PhotoRepos();
            Photo photo = photoRepos.getById(stock.id_image);

            // Поиск поломок
            var damageRepos = new DamageRepos();
            List<Damage> damages = damageRepos.getByStockID(stock.id).ToList();

            // Удаление повреждений
            foreach (Damage damage in damages)
            {
                damageRepos.Delete(damage.id);
            }
            damageRepos.Save();

            // Удаление инвентаря
            stockRepos.Delete(stock.id);
            stockRepos.Save();

            // Удаление фото
            photoRepos.Delete(photo.id);
            photoRepos.Save();

            // Очистка поля
            clear_stock_passport();
            load_all_stock();
        }

        /// <summary>
        /// Вывод информации об арендателе
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label5_click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Обработка двойного нажатия на инвентарь
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            set_status_stock(true);
        }

        /// <summary>
        /// Обработка двойного нажатия на помещение
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView4_DoubleClick(object sender, EventArgs e)
        {
            set_status_gym(true);
        }
    }
}
