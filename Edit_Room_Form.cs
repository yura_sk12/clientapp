﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    /// <summary>
    /// Класс формы для редактирования помещения
    /// </summary>
    public partial class Edit_Room_Form : Form
    {
        /// <summary>
        /// Конструктор - выполняется только инициализация компонентов
        /// </summary>
        public Edit_Room_Form()
        {
            InitializeComponent();
        }

        /// <summary>
        /// id комнаты
        /// </summary>
        public string room_id
        {
            get
            {
                return label3.Text;
            }
            set
            {
                label3.Text = value;
            }
        }

        /// <summary>
        /// Путь к картинке
        /// </summary>
        public string room_img_path
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        /// <summary>
        /// Адрес
        /// </summary>
        public string address
        {
            get
            {
                return textBox2.Text;
            }
            set
            {
                textBox2.Text = value;
            }
        }

        /// <summary>
        /// Стоимость
        /// </summary>
        public string cost
        {
            get
            {
                return textBox3.Text;
            }
            set
            {
                textBox3.Text = value;
            }
        }

        /// <summary>
        /// Список характеристик
        /// </summary>
        public ListView.ListViewItemCollection listViewItems
        {
            get
            {
                return listView2.Items;
            }
            set
            {
                listView2.Items.AddRange(value);
            }
        }

        /// <summary>
        /// Обработка нажатия Ок
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0)
            {
                textBox1.BackColor = Color.Red;
                return;
            }
            if (textBox2.Text.Length == 0)
            {
                textBox2.BackColor = Color.Red;
                return;
            }
            if (!ValidDouble(textBox3.Text)) return;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Обработка добавления описания помещения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox5.Text.Length == 0)
            {
                textBox5.BackColor = Color.Red;
                return;
            }

            this.listView2.Items.Add(this.textBox5.Text.ToString());
            textBox5.Clear();
        }

        /// <summary>
        /// Обработка удаления описания помещения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem eachItem in this.listView2.SelectedItems)
            {
                this.listView2.Items.Remove(eachItem);
            }
        }


        /// <summary>
        /// Обработка кнопки выбора картинки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.FileName = this.textBox1.Text;
            openFileDialog.Title = "Select a Image File";
            openFileDialog.Filter = "Image files(*.jpg)|*.jpg|" +
                "Image files(*.jpeg)|*.jpeg|" +
                "Image files(*.png)|*.png|" +
                "All files(*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = openFileDialog.FileName;
            textBox1.Text = filename.ToString();
        }

        /// <summary>
        /// Обработка нажатия на кнопку Close
        /// </summary>
        /// <param name="sender">Кнопка close</param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Валидация стоимости выполнена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox3_Validated(object sender, EventArgs e)
        {
            textBox3.BackColor = Color.White;
        }

        /// <summary>
        /// Валидация стоимости
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox3_Validating(object sender, CancelEventArgs e)
        {
            if ((textBox3.Text.IndexOf('.') != textBox3.Text.LastIndexOf('.')) || textBox3.Text.Length == 0 || !ValidDouble(textBox3.Text))
            {
                textBox3.BackColor = Color.Red;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Событие на изменение стоимости
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (!ValidDouble(textBox3.Text))
            {
                textBox3.BackColor = Color.Pink;
            }
            else
            {
                textBox3.BackColor = Color.White;
            }
        }

        /// <summary>
        /// Обработка ввода стоимости
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!(char.IsDigit(ch) || char.IsControl(ch) || ch == ','))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Обработка закрытия формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Edit_Room_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }

        /// <summary>
        /// Валидация адреса выполнена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox2_Validated(object sender, EventArgs e)
        {
            textBox2.BackColor = Color.White;
        }

        /// <summary>
        /// Валидация адреса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            if (textBox2.Text.Length == 0)
            {
                textBox2.BackColor = Color.Red;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Обработка изменения поля
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            textBox5.BackColor = Color.White;
        }

        /// <summary>
        /// Обработка изменения поля
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.BackColor = Color.White;
        }

        /// <summary>
        /// Проверка стоки с вещественным числом на возможность конвертации
        /// </summary>
        /// <param name="str_in"></param>
        /// <returns></returns>
        private bool ValidDouble(string str_in)
        {
            double out_cost = -1;
            return Double.TryParse(textBox3.Text, out out_cost);
        }

    }
}