﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace ClientApp
{
    /// <summary>
    /// Форма редактирования инвентаря
    /// </summary>
    public partial class Edit_Invent_Form : Form
    {
        /// <summary>
        /// Код кнопок отмена (1) и Ок(0) если 3 то кнопки нажаты небыли
        /// </summary>
        private int result_code = 3;

        /// <summary>
        /// Конструктор, выполняется инициализация только компонентов формы
        /// </summary>
        public Edit_Invent_Form()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Проверка штрафа
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private bool check_digit(string str)
        {
            string pattern = @"\d.\d$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(str);
        }

        /// <summary>
        /// Код кнопок отмена (1) и Ок(0) если 3 то кнопки нажаты небыли
        /// </summary>
        /// <returns></returns>
        public int get_result_code()
        {
            return result_code;
        }
        
        /// <summary>
        /// id инвентаря
        /// </summary>
        public long invent_id
        {
            get
            {
                return Convert.ToInt64(label9.Text);
            }
            set
            {
                label9.Text = value.ToString();
            }
        }

        /// <summary>
        /// Путь к изображению инвентаря
        /// </summary>
        public string invent_img_path
        {
            get {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        /// <summary>
        /// Наименование инвентаря
        /// </summary>
        public string invent_name
        {
            get
            {
                return textBox2.Text;
            }
            set
            {
                textBox2.Text = value;
            }
        }

        /// <summary>
        /// Стоимость аренды инвентаря
        /// </summary>
        public string invent_cost
        {
            get
            {
                return textBox3.Text;
            }
            set
            {
                textBox3.Text = value;
            }
        }

        /// <summary>
        /// Точка выдачи инвентаря
        /// </summary>
        public string invent_point_to_rent
        {
            get
            {
                return textBox4.Text;
            }
            set
            {
                textBox4.Text = value;
            }
        }

        /// <summary>
        /// Список повреждений инвентаря
        /// </summary>
        public ListView.ListViewItemCollection invet_damage
        {
            get
            {
                return listView2.Items;
            }
        }

        /// <summary>
        /// Выбор фотографии инвентаря
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files(*.jpeg)|*.jpeg|" +
                "Image files(*.jpg)|*.jpg|" +
                "Image files(*.png)|*.png|" +
                "All files(*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = openFileDialog.FileName;
            textBox1.Text = filename.ToString();
        }

        /// <summary>
        /// Нажатие кнопки Ок
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            this.result_code = 0;
            if (textBox1.Text.Length == 0)
            {
                textBox1.BackColor = Color.Red;
                return;
            }
            if (textBox2.Text.Length == 0)
            {
                textBox2.BackColor = Color.Red;
                return;
            }
            if (!ValidDouble(textBox3.Text)) return;
            if (textBox4.Text.Length == 0)
            {
                textBox4.BackColor = Color.Red;
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Нажатие кнопки отмена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            this.result_code = 1;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Добавление дефекта 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == "")
            {
                textBox5.BackColor = Color.Red;
                return;
            }
            if (textBox6.Text == "" || !ValidDouble(textBox6.Text))
            {
                textBox6.BackColor = Color.Red;
                return;
            }
            Select_rents_form rent_forn = new Select_rents_form();
            rent_forn.ShowDialog();
            if (rent_forn.DialogResult != DialogResult.OK)
            {
                return;
            }
            Renter renter = rent_forn.renter;
            string description = textBox5.Text;
            string fine = textBox6.Text;
            string[] newstr = {description, renter.fio, renter.passport.ToString(), fine, renter.id.ToString()};
            this.listView2.Items.Add(new ListViewItem(newstr));
        }

        /// <summary>
        /// При изменении текста изменяет фон на белый
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            textBox5.BackColor = Color.White;
        }

        /// <summary>
        /// Валидация стоимости штрафа
        /// </summary>
        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (!ValidDouble(textBox6.Text))
            {
                textBox6.BackColor = Color.Pink;
            }
            else
            {
                textBox6.BackColor = Color.White;
            }
        }

        /// <summary>
        /// Удаление строки из ListView
        /// </summary>
        /// <param name="sender">Button Удалить</param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection selected_items = listView2.SelectedItems;
            if (selected_items.Count == 0)
            {
                return;
            }
            if (selected_items == null)
            {
                return;
            }
            listView2.Items.Remove(selected_items[0]);
        }

        /// <summary>
        /// Валидация названия
        /// </summary>
        private void textBox2_Validated(object sender, EventArgs e)
        {
            textBox2.BackColor = Color.White;
        }
        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            if (textBox2.Text.Length == 0)
            {
                textBox2.BackColor = Color.Red;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Валидация стоимости
        /// </summary>
        private void textBox3_Validated(object sender, EventArgs e)
        {
            textBox3.BackColor = Color.White;
        }
        private void textBox3_Validating(object sender, CancelEventArgs e)
        {
            if ((textBox3.Text.IndexOf('.') != textBox3.Text.LastIndexOf('.')) || textBox3.Text.Length == 0 || !ValidDouble(textBox3.Text))
            {
                textBox3.BackColor = Color.Red;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Нажатие кнопки клавиатуры на поле стоимости 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!(char.IsDigit(ch) || char.IsControl(ch) || ch == ','))
            {
                e.Handled = true;
            }
        }
        /// <summary>
        /// Изменение текста в поле стоимости 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (!ValidDouble(textBox3.Text))
            {
                textBox3.BackColor = Color.Pink;
            }
            else
            {
                textBox3.BackColor = Color.White;
            }
        }
        /// <summary>
        /// Валидация пункта выдачи
        /// </summary>
        private void textBox4_Validated(object sender, EventArgs e)
        {
            textBox4.BackColor = Color.White;

        }

        /// <summary>
        /// Валидация пункта выдачи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox4_Validating(object sender, CancelEventArgs e)
        {
            if (textBox4.Text.Length == 0)
            {
                textBox4.BackColor = Color.Red;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Выключение даже при не выполненой валидации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Edit_Invent_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }

        /// <summary>
        /// Изменение текста фона на белый при изменении текста в поле пути к изображению
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.BackColor = Color.White;
        }

        /// <summary>
        /// Проврка на возможность преобразования в double
        /// </summary>
        /// <param name="str_in">Строка для проверки</param>
        /// <returns>Результат попытки преобразования</returns>
        private bool ValidDouble(string str_in)
        {
            double out_cost = -1;
            return Double.TryParse(str_in, out out_cost);
            //return check_digit(str_in);
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!(char.IsDigit(ch) || char.IsControl(ch) || ch == ','))
            {
                e.Handled = true;
            }
        }
    }
}
