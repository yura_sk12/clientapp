﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    /// <summary>
    /// Форма для аренды помещения или инвентаря
    /// </summary>
    public partial class Arenda_Form : Form
    {
        /// <summary>
        /// Глабальная переменная хранения Арендатора
        /// </summary>
        private Renter global_renter = null;

        /// <summary>
        /// Конструктор инициализирует только компоненты формы
        /// </summary>
        public Arenda_Form()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Арендатор - Renter
        /// </summary>
        public Renter renter
        {
            get
            {
                return global_renter;
            }
        }

        /// <summary>
        /// Дата начала аренды
        /// </summary>
        public DateTime date_start
        {
            get
            {
                return dateTimePicker4.Value.Date;
            }
        }

        /// <summary>
        /// Дата конца аренды
        /// </summary>
        public DateTime date_end
        {
            get
            {
                return dateTimePicker3.Value.Date;
            }
        }

        /// <summary>
        /// Наименование инвентаря
        /// </summary>
        public string name
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        /// <summary>
        /// id инвентаря
        /// </summary>
        public string article
        {
            get
            {
                return textBox3.Text;
            }
            set
            {
                textBox3.Text = value;
            }
        }

        /// <summary>
        /// Выбор арендатора
        /// </summary>
        /// <param name="sender">Button выбора арендатора</param>
        /// <param name="e">Класс событий</param>
        private void button3_Click(object sender, EventArgs e)
        {
            Select_rents_form rent_forn = new Select_rents_form();
            rent_forn.ShowDialog();
            if (rent_forn.DialogResult == DialogResult.OK)
            {
                Renter renter = rent_forn.renter;
                global_renter = renter;
                listView2.Items.Clear();
                string[] str = { renter.id.ToString(), renter.fio, renter.phone };
                listView2.Items.Add(new ListViewItem(str));
            }
        }

        /// <summary>
        /// Осуществляет сравнение дат в полях формы
        /// </summary>
        /// <returns>Разрешено ли такие даты истанавливать</returns>
        private bool compare_date()
        {
            bool res = check_with_current_date(date_start);
            if (!res)
            {
                return false;
            }
            res = check_with_current_date(date_end);
            if (!res)
            {
                return false;
            }
            if (date_start > date_end)
            {
                MessageBox.Show("Дата начала аренды не может быть меньше даты окончания");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Нажание ok
        /// </summary>
        /// <param name="sender">Button ok</param>
        /// <param name="e">Класс событий</param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (listView2.Items.Count != 1)
            {
                MessageBox.Show("Ошибка выберите арендатора");
                return;
            }
            bool res = compare_date();
            if (!res)
                return;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Нажата Отмена
        /// </summary>
        /// <param name="sender">Button отмена</param>
        /// <param name="e">Класс событий</param>
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Сравнение даты с текущей
        /// </summary>
        /// <param name="date_sender">больше или меньше текущей даты</param>
        private bool check_with_current_date(DateTime date_sender)
        {
            DateTime current_date = DateTime.Now.Date;
            if (current_date > date_sender)
            {
                MessageBox.Show("Дата аренды или бронирования \r\nне может быть меньше текущей даты");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Событие на изменение даты от
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dateTimePicker4_ValueChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Событие на изменение даты до
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {
        }
    }
}
