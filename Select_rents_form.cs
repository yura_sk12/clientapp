﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    /// <summary>
    /// Фомра выбора арендатора
    /// </summary>
    public partial class Select_rents_form : Form
    {
        private Renter global_renter = null;

        /// <summary>
        /// Конструктор - инициализируются компоненты и загружаетя список арендаторов из базы
        /// </summary>
        public Select_rents_form()
        {
            InitializeComponent();
            update_listView();
        }

        /// <summary>
        /// Арендатор
        /// </summary>
        public Renter renter
        {
            get
            {
                return global_renter;
            }
        }

        /// <summary>
        /// Добавление нового арендатора
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            Add_renter_form add_renter = new Add_renter_form();
            add_renter.ShowDialog();
            if (add_renter.DialogResult != DialogResult.OK)
            {
                return;
            }

            RenterRepos renterRepos = new RenterRepos();
            Renter renter = new Renter();
            PhotoRepos photoRepos = new PhotoRepos();
            Photo photo = new Photo();

            // Добавление фото
            photo.path = add_renter.renter_img_path;
            photoRepos.Create(photo);
            photoRepos.Save();

            // Добавление рентера
            renter.fio = add_renter.renter_fio;
            renter.phone = add_renter.renter_phone;
            renter.id_image = photo.id;
            renter.passport = add_renter.renter_passport;
            renterRepos.Create(renter);
            renterRepos.Save();

            // Обновление списка
            update_listView();

            // Обновление ключа
            //renter.id = Convert.ToInt64(add_renter.renter_passport);
            //renterRepos.Update(renter);
            //renterRepos.Save();

            // Todo Выбор рентора в списке
            //ListView.ListViewItemCollection items = this.listView2.Items;
            //ListViewItem[] item = items.Find(Convert.ToString(renter.id), false);
            //load_photo(renter);


        }

        /// <summary>
        /// Загрузка изображения в форму по переданному Renter
        /// </summary>
        /// <param name="renter">Арендатор чье фото необходимо вывести</param>
        private void load_photo(Renter renter)
        {
            try
            {
                this.pictureBox1.Image = new Bitmap(new PhotoRepos().getById(renter.id_image).path);
            }
            catch
            {
                this.pictureBox1.Image = null;
            }
        }

        /// <summary>
        /// Обновление списка арендаторов
        /// </summary>
        private void update_listView()
        {
            RenterRepos renterRepos = new RenterRepos();
            List<Renter> renters = renterRepos.getAll().ToList();

            listView2.Items.Clear();
            foreach (Renter renter in renters)
            {
                string[] str = {renter.id.ToString(), renter.fio, renter.phone, renter.passport};
                listView2.Items.Add(new ListViewItem(str));
            }
        }

        /// <summary>
        /// Обработка выделения арендатора из списка (загрузка паспорта арендатора)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView listView = (ListView)sender;
            ListView.SelectedListViewItemCollection selected_item = listView.SelectedItems;
            clear_rent_passport();
            if (selected_item.Count == 0)
            {
                return;
            }
            if (selected_item != null)
            {
                // Ищем Renter по выделенному ID
                RenterRepos context = new RenterRepos();
                Renter renter = context.getById(Convert.ToInt64(selected_item[0].Text));

                if (renter == null)
                {
                    return;
                }

                global_renter = renter;
                this.load_photo(renter);
                this.button2.Enabled = true;
            }
        }

        /// <summary>
        /// Очистка пасспорта арендатора
        /// </summary>
        private void clear_rent_passport()
        {
            this.pictureBox1.Image = null;
        }

        /// <summary>
        /// Нажатие Ок
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Нажатие отмены
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
