var class_client_app_1_1_gym__desc_repos =
[
    [ "Gym_descRepos", "class_client_app_1_1_gym__desc_repos.html#a01967ca70bce1859a2f1ab10599b878e", null ],
    [ "Create", "class_client_app_1_1_gym__desc_repos.html#a6ed9540d8b02e171026172116a777f19", null ],
    [ "Delete", "class_client_app_1_1_gym__desc_repos.html#af143de2113f05552870ad59ba605f05e", null ],
    [ "Dispose", "class_client_app_1_1_gym__desc_repos.html#a85f622ebd1913391bf03431df8b33d5a", null ],
    [ "Dispose", "class_client_app_1_1_gym__desc_repos.html#ac6a213034268066e7c4efd7827fb2a2c", null ],
    [ "getAll", "class_client_app_1_1_gym__desc_repos.html#ad455e51e92afa5adba3015598140e2d0", null ],
    [ "getByGymID", "class_client_app_1_1_gym__desc_repos.html#ae82c3f1aea236e23491dffba60d4c5b1", null ],
    [ "getById", "class_client_app_1_1_gym__desc_repos.html#afe5d46d72e48ee9723dbaf0126f7fe59", null ],
    [ "Save", "class_client_app_1_1_gym__desc_repos.html#ad797717fcb5f0b3706d05e2b0750e3f0", null ],
    [ "Update", "class_client_app_1_1_gym__desc_repos.html#a26fc5d448f6abc3461139191743c6d87", null ],
    [ "disposed", "class_client_app_1_1_gym__desc_repos.html#a95ce3c02ef17beb8bf0fbb1c24c90b81", null ],
    [ "gym_Desc_Context", "class_client_app_1_1_gym__desc_repos.html#a2d3d0ad7c52df2a2346e86f32494af27", null ]
];