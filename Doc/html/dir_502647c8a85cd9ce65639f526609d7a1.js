var dir_502647c8a85cd9ce65639f526609d7a1 =
[
    [ "DamageRepos.cs", "_damage_repos_8cs.html", [
      [ "DamageRepos", "class_client_app_1_1_damage_repos.html", "class_client_app_1_1_damage_repos" ]
    ] ],
    [ "Gym_descRepos.cs", "_gym__desc_repos_8cs.html", [
      [ "Gym_descRepos", "class_client_app_1_1_gym__desc_repos.html", "class_client_app_1_1_gym__desc_repos" ]
    ] ],
    [ "Gym_rentRepos.cs", "_gym__rent_repos_8cs.html", [
      [ "Gym_rentRepos", "class_client_app_1_1_gym__rent_repos.html", "class_client_app_1_1_gym__rent_repos" ]
    ] ],
    [ "GymRepos.cs", "_gym_repos_8cs.html", [
      [ "GymRepos", "class_client_app_1_1_gym_repos.html", "class_client_app_1_1_gym_repos" ]
    ] ],
    [ "PhotoRepos.cs", "_photo_repos_8cs.html", [
      [ "PhotoRepos", "class_client_app_1_1_photo_repos.html", "class_client_app_1_1_photo_repos" ]
    ] ],
    [ "RenterRepos.cs", "_renter_repos_8cs.html", [
      [ "RenterRepos", "class_client_app_1_1_renter_repos.html", "class_client_app_1_1_renter_repos" ]
    ] ],
    [ "StockRentRepos.cs", "_stock_rent_repos_8cs.html", [
      [ "StockRentRepos", "class_client_app_1_1_stock_rent_repos.html", "class_client_app_1_1_stock_rent_repos" ]
    ] ],
    [ "StockRepos.cs", "_stock_repos_8cs.html", [
      [ "StockRepos", "class_client_app_1_1_stock_repos.html", "class_client_app_1_1_stock_repos" ]
    ] ]
];