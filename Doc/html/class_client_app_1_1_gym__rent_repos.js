var class_client_app_1_1_gym__rent_repos =
[
    [ "Gym_rentRepos", "class_client_app_1_1_gym__rent_repos.html#abeb7a592101fe584040ddfb14ee3da5a", null ],
    [ "Create", "class_client_app_1_1_gym__rent_repos.html#a634995dffd8cffddc5fbeecfd510a0c4", null ],
    [ "Delete", "class_client_app_1_1_gym__rent_repos.html#ad7cab9ad2f0646ad99e59d17cb79c0b1", null ],
    [ "Dispose", "class_client_app_1_1_gym__rent_repos.html#aaa38f8e8dd23e4f132cb27642f15cd4e", null ],
    [ "Dispose", "class_client_app_1_1_gym__rent_repos.html#aeb1ca4eb46c64dde613937c5ec81ec82", null ],
    [ "getAll", "class_client_app_1_1_gym__rent_repos.html#ab3a7b7f8cd1fc28699de8827c260e452", null ],
    [ "getById", "class_client_app_1_1_gym__rent_repos.html#ad19695daabe25a44078aff31a3061ffc", null ],
    [ "Save", "class_client_app_1_1_gym__rent_repos.html#a3242722520ae6c7353c3a1c6f95c6ad3", null ],
    [ "Update", "class_client_app_1_1_gym__rent_repos.html#a8b8c74ae9469d36d5a1211c126c78cf8", null ],
    [ "disposed", "class_client_app_1_1_gym__rent_repos.html#aae5161a67a2c63b4672063abf1e39890", null ],
    [ "gym_rent_Context", "class_client_app_1_1_gym__rent_repos.html#a4475b4e69602da9c100a1544ffa560b8", null ]
];