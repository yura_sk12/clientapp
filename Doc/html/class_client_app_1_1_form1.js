var class_client_app_1_1_form1 =
[
    [ "Form1", "class_client_app_1_1_form1.html#acd467f64c79f047b1884bed57f9bbbd0", null ],
    [ "button10_Click", "class_client_app_1_1_form1.html#a15eb3f6c73406eb950b968dfb6ffeb87", null ],
    [ "button11_Click", "class_client_app_1_1_form1.html#af0440fee1eea5b87e79a1c27498f0bf1", null ],
    [ "button12_Click", "class_client_app_1_1_form1.html#a4d3c1b6cdd6a0920c930ccec6400311d", null ],
    [ "button13_Click", "class_client_app_1_1_form1.html#a6ed3bfa6bdcfc575164974598412004f", null ],
    [ "button14_Click", "class_client_app_1_1_form1.html#a971bdffd4b914aec418bfacc1cd52a79", null ],
    [ "button1_Click", "class_client_app_1_1_form1.html#ac6a38c24c235a82108b09190e8a55894", null ],
    [ "button2_Click", "class_client_app_1_1_form1.html#abe3c5ca651ed80f0bcf7bb5e5042541c", null ],
    [ "button3_Click", "class_client_app_1_1_form1.html#a9eee7054e62ba9bf2f9d181d143e3cd8", null ],
    [ "button4_Click", "class_client_app_1_1_form1.html#aa97ee15800321e982e6f7d5b00e98f5f", null ],
    [ "button5_Click", "class_client_app_1_1_form1.html#a708f16fac948032433f4b96b99791057", null ],
    [ "button6_Click", "class_client_app_1_1_form1.html#aebc44aa2e62ba03e04978fc0f1edc70f", null ],
    [ "button9_Click", "class_client_app_1_1_form1.html#a2003832edda78fd3f8c60bac9dbc34f7", null ],
    [ "check_for_rent_gym", "class_client_app_1_1_form1.html#aabaa12dcd8dc7a1b935e0d3479a27601", null ],
    [ "check_for_rent_stock", "class_client_app_1_1_form1.html#acd7d1c5386fdcd0bc497ce85c0d53825", null ],
    [ "clear_gym_passport", "class_client_app_1_1_form1.html#a4517151e13959a80f0532d71da4fa950", null ],
    [ "clear_stock_passport", "class_client_app_1_1_form1.html#a88c76b89f0af7b704e07803f3bf0c904", null ],
    [ "Dispose", "class_client_app_1_1_form1.html#aa1084ff84b9cd116c8fd5cc55dc801ea", null ],
    [ "get_gym_status", "class_client_app_1_1_form1.html#a162779009657667b8ea10b4bdf4bfc01", null ],
    [ "get_stock_status", "class_client_app_1_1_form1.html#acaedb9c3421fa80d37c057d7674b2187", null ],
    [ "InitializeComponent", "class_client_app_1_1_form1.html#a4fa64ceb1b97dac480e286989d13d488", null ],
    [ "label5_click", "class_client_app_1_1_form1.html#a45b418d3042acb1bf52561fc495c5bb9", null ],
    [ "listView1_DoubleClick", "class_client_app_1_1_form1.html#a5c952c4de05f575e6e0c785be2945bb0", null ],
    [ "listView1_SelectedIndexChanged", "class_client_app_1_1_form1.html#ae1c4e72ef7228e61819ec47b2d82e59a", null ],
    [ "listView4_DoubleClick", "class_client_app_1_1_form1.html#a72c2b4da9962b1ca076ed18054cf69ff", null ],
    [ "listView4_SelectedIndexChanged", "class_client_app_1_1_form1.html#a20fc0e6d4e216a16ef89b4cc2e59e5de", null ],
    [ "load_all_stock", "class_client_app_1_1_form1.html#a5c328ef0febe518ecc1eb8ddb374a8e6", null ],
    [ "LoadDataToRoomListView", "class_client_app_1_1_form1.html#aed89ec64937ffa96be0d005e200971ba", null ],
    [ "set_status_gym", "class_client_app_1_1_form1.html#aeee800f938a99bdbbbf3fa85e99d978c", null ],
    [ "set_status_stock", "class_client_app_1_1_form1.html#aa20509bee094ce7ff0a9cac3a04332a0", null ],
    [ "arenda", "class_client_app_1_1_form1.html#a3dc1b5914f8afc3880f6af05afb7e898", null ],
    [ "articul", "class_client_app_1_1_form1.html#a35ba5ba5f4422a5e6eded1d1a5ac25b1", null ],
    [ "breaks", "class_client_app_1_1_form1.html#ab557db7da7d5ed7bce9705bfb5dca031", null ],
    [ "button1", "class_client_app_1_1_form1.html#a54737536ab1aa9a2883368ea5c17b92b", null ],
    [ "button10", "class_client_app_1_1_form1.html#ae2cba739a7fd2bdf860134d97828ed2d", null ],
    [ "button11", "class_client_app_1_1_form1.html#aef4238f6085f7df333c8b86f235b08dc", null ],
    [ "button12", "class_client_app_1_1_form1.html#acbde5f925fc2e4ed581e141fad02b4a4", null ],
    [ "button13", "class_client_app_1_1_form1.html#a5cdae8daa4d16e57ab65ca00562535a4", null ],
    [ "button14", "class_client_app_1_1_form1.html#a5414d6bfa9b2c8c933e8baaea9022a00", null ],
    [ "button2", "class_client_app_1_1_form1.html#ab69254aa789a8a0bdc93348c9ff4cb70", null ],
    [ "button3", "class_client_app_1_1_form1.html#a81be5606a27146e59ca7af4bfdea7d68", null ],
    [ "button4", "class_client_app_1_1_form1.html#a30363102d6f22544ae86113e17f200f3", null ],
    [ "button5", "class_client_app_1_1_form1.html#aff867d233c0b7c85f82f0ac58ac8bee6", null ],
    [ "button6", "class_client_app_1_1_form1.html#a712dd4e853b7448506f1854bca775471", null ],
    [ "button9", "class_client_app_1_1_form1.html#a0854ec6851aae56279ac93b008815a1f", null ],
    [ "columnHeader1", "class_client_app_1_1_form1.html#a32870053ad8417a1ed8ff1b8bc397d57", null ],
    [ "columnHeader2", "class_client_app_1_1_form1.html#acdd805533603990a26307a8a858d7eae", null ],
    [ "columnHeader3", "class_client_app_1_1_form1.html#a46fd2c6a920bbf22d54abf1cce0f551f", null ],
    [ "columnHeader4", "class_client_app_1_1_form1.html#ac12ba350d4f75c1fe63255cdc468b2ce", null ],
    [ "columnHeader5", "class_client_app_1_1_form1.html#ad9b15c00452a1500292e791209f29560", null ],
    [ "components", "class_client_app_1_1_form1.html#a2359790d34a85273e9608c5646f44728", null ],
    [ "fio", "class_client_app_1_1_form1.html#af9bd7b1de66c958b3d57cc4fe279b947", null ],
    [ "groupBox1", "class_client_app_1_1_form1.html#a55c0bbd0c4c2f52b51d811b5a7185f4e", null ],
    [ "groupBox2", "class_client_app_1_1_form1.html#ae26a12d86ddf24b4860bfff2c20840fc", null ],
    [ "inventar", "class_client_app_1_1_form1.html#ad6bc819d6cbb84c6787006761b946168", null ],
    [ "inventName", "class_client_app_1_1_form1.html#a5fb91af6a4d79e0e3c6b15fdf039f5a2", null ],
    [ "label1", "class_client_app_1_1_form1.html#a45f4de308842804b3cfdb105d5e2b031", null ],
    [ "label10", "class_client_app_1_1_form1.html#afdbad86556dd491b904060d43ec397d8", null ],
    [ "label11", "class_client_app_1_1_form1.html#af961164710601fa233cf983b67e65429", null ],
    [ "label12", "class_client_app_1_1_form1.html#a9a6157efe07f6b26da1b942aaca3ad24", null ],
    [ "label2", "class_client_app_1_1_form1.html#a3aaa1b41a0ca9dcccb465c702a5c5c62", null ],
    [ "label3", "class_client_app_1_1_form1.html#a902ad3157f28870b5d45c45d2f1ba0fc", null ],
    [ "label4", "class_client_app_1_1_form1.html#aecf6923b733ab60a5ec4b0f5d8ec6a44", null ],
    [ "label5", "class_client_app_1_1_form1.html#a29e2b45c4b0ab2f9d261d65209f0ba96", null ],
    [ "label6", "class_client_app_1_1_form1.html#a496526c924bcd0fe18120570468da9ea", null ],
    [ "label7", "class_client_app_1_1_form1.html#a748a2ff4e73c627acb326ab1f5d3b531", null ],
    [ "label8", "class_client_app_1_1_form1.html#ac73a4ee5651d8ed6c2503b5bbba16ffe", null ],
    [ "label9", "class_client_app_1_1_form1.html#af14ccb1847e31373c8de18514f900d89", null ],
    [ "listView1", "class_client_app_1_1_form1.html#abda796852683ad1a1225f76d1589b254", null ],
    [ "listView2", "class_client_app_1_1_form1.html#a7dfd63e4a41e35e6c7707c1eb6429c69", null ],
    [ "listView3", "class_client_app_1_1_form1.html#a42e8c4cdbea36742c15fdbf0351ced52", null ],
    [ "listView4", "class_client_app_1_1_form1.html#abe37f24e9b143a7867feb1264eb8b06d", null ],
    [ "pictureBox1", "class_client_app_1_1_form1.html#a5e4fdc3d5f7b38eff269ebb0d39ab11b", null ],
    [ "pictureBox2", "class_client_app_1_1_form1.html#ae9fc5f18d3035c85875934747a593eab", null ],
    [ "price", "class_client_app_1_1_form1.html#aa07a63e175922d9d10fa95865e3b02ea", null ],
    [ "punkt", "class_client_app_1_1_form1.html#ab385b7e736a299ebbe5399503c796e1f", null ],
    [ "shtraf", "class_client_app_1_1_form1.html#a807b1b105c72ff201094eb294f65e5b8", null ],
    [ "state", "class_client_app_1_1_form1.html#a7c3775c25ca0989b8aa0ddedcf319e80", null ],
    [ "tabControl1", "class_client_app_1_1_form1.html#a6f78bea23966dcdd6bf95c9b5a3e8d38", null ]
];