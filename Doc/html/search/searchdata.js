var indexSectionsWithContent =
{
  0: "abcdefgilnoprstuv",
  1: "adefgprs",
  2: "c",
  3: "abcdefgiloprstuv",
  4: "abcdfgilprst",
  5: "acdfgilnprs",
  6: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Указатель",
  1: "Классы",
  2: "Пространства имен",
  3: "Функции",
  4: "Переменные",
  5: "Свойства",
  6: "Страницы"
};

