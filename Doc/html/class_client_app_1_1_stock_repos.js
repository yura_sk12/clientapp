var class_client_app_1_1_stock_repos =
[
    [ "StockRepos", "class_client_app_1_1_stock_repos.html#ad224aba7a7bd6b0eb46d4b231ae30128", null ],
    [ "Create", "class_client_app_1_1_stock_repos.html#aeaae81d45dd1c3fc5ab6a9996ab158c9", null ],
    [ "Delete", "class_client_app_1_1_stock_repos.html#a1c710a5a9e81abab9118fcacb41e05f2", null ],
    [ "Dispose", "class_client_app_1_1_stock_repos.html#a8357e8252e3a3d181d5dc0102c86d88d", null ],
    [ "Dispose", "class_client_app_1_1_stock_repos.html#a1d228cc7b13a07d0beb47b0edede2442", null ],
    [ "getAllStock", "class_client_app_1_1_stock_repos.html#adb22f8f51f70e1df63b7aa757c87b530", null ],
    [ "getById", "class_client_app_1_1_stock_repos.html#adb9408674c6bcec4221788d4bdb26196", null ],
    [ "Save", "class_client_app_1_1_stock_repos.html#adcc9a534e79f193752d88f66199cbdff", null ],
    [ "Update", "class_client_app_1_1_stock_repos.html#a747f550e16f612025053b327b37e81b3", null ],
    [ "disposed", "class_client_app_1_1_stock_repos.html#ae771a6a5c34af8b6038add802e38c7af", null ],
    [ "stockContext", "class_client_app_1_1_stock_repos.html#a4d00fae2f915967d82ea0854ae701522", null ]
];