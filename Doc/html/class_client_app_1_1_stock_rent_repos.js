var class_client_app_1_1_stock_rent_repos =
[
    [ "StockRentRepos", "class_client_app_1_1_stock_rent_repos.html#a7edc5d0bdf52fb318ab62b336475dc3e", null ],
    [ "Create", "class_client_app_1_1_stock_rent_repos.html#a8bff9411089896d18b891f2e4e40a7a7", null ],
    [ "Delete", "class_client_app_1_1_stock_rent_repos.html#a2956ceb333af935cb18c33d69acc27fa", null ],
    [ "Dispose", "class_client_app_1_1_stock_rent_repos.html#a297c17958cc2ef452c6f7fd09771ec0f", null ],
    [ "Dispose", "class_client_app_1_1_stock_rent_repos.html#a42f757ec9083218cc9781e49f8938e0d", null ],
    [ "getAll", "class_client_app_1_1_stock_rent_repos.html#a0363214264e4be129b966659b535f5d0", null ],
    [ "getById", "class_client_app_1_1_stock_rent_repos.html#aa43ddfd59521cf72209d4814d70b9586", null ],
    [ "Save", "class_client_app_1_1_stock_rent_repos.html#afaf4c1319111cd41643b68fcd480a93c", null ],
    [ "Update", "class_client_app_1_1_stock_rent_repos.html#a068d5feb3ccc29d5ee63a1547b5ba5e8", null ],
    [ "disposed", "class_client_app_1_1_stock_rent_repos.html#a470b743c13c2d66566df9e82b61c0fc2", null ],
    [ "stockRentContext", "class_client_app_1_1_stock_rent_repos.html#ad4e2392e122b373d06afa46921711b18", null ]
];