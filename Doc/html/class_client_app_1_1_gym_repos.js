var class_client_app_1_1_gym_repos =
[
    [ "GymRepos", "class_client_app_1_1_gym_repos.html#a12cf518bf45fb5deb7f79a8c5b706209", null ],
    [ "Create", "class_client_app_1_1_gym_repos.html#af144330c1429f9d52f1b84d9fc0c1684", null ],
    [ "Delete", "class_client_app_1_1_gym_repos.html#a06117a5da48b884a1810be236986ec40", null ],
    [ "Dispose", "class_client_app_1_1_gym_repos.html#a6d3b238b8cfbdb58a245c1189021db58", null ],
    [ "Dispose", "class_client_app_1_1_gym_repos.html#ae6d846dc9506019bfb0c76e4303b84e1", null ],
    [ "getAll", "class_client_app_1_1_gym_repos.html#a6383c676c08f0e9dcc7bb454eba66c18", null ],
    [ "getById", "class_client_app_1_1_gym_repos.html#a7ffb675e006d3e020e9c32dc239b7519", null ],
    [ "Save", "class_client_app_1_1_gym_repos.html#a48c1fb337574cbabffb875f1f5578f52", null ],
    [ "Update", "class_client_app_1_1_gym_repos.html#ae313f8503fa64fcacf1158132be0c9f7", null ],
    [ "disposed", "class_client_app_1_1_gym_repos.html#a3c5d521bebeebce290556e01fab26639", null ],
    [ "gymContext", "class_client_app_1_1_gym_repos.html#ad4d05bd1bac28b401c89c4f2186d6f0f", null ]
];