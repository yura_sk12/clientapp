var class_client_app_1_1_damage_repos =
[
    [ "DamageRepos", "class_client_app_1_1_damage_repos.html#a23a203d3cd1a2a033981e9cd43b831da", null ],
    [ "Create", "class_client_app_1_1_damage_repos.html#aba73ffa5262c3018812a35b42205fc5a", null ],
    [ "Delete", "class_client_app_1_1_damage_repos.html#ae54aaf0a03ca696458d5cc11003c1d91", null ],
    [ "Dispose", "class_client_app_1_1_damage_repos.html#a6e6c0340612700c7e5f089c6e3e2493b", null ],
    [ "Dispose", "class_client_app_1_1_damage_repos.html#a2bbff6d503913db4f813a0612861cb41", null ],
    [ "getAll", "class_client_app_1_1_damage_repos.html#a2bd8d2d377a06fcba65bc034e762aa52", null ],
    [ "getById", "class_client_app_1_1_damage_repos.html#a984fa30d3044cdb85104a16754270ce9", null ],
    [ "getByStockID", "class_client_app_1_1_damage_repos.html#aed9423e0ec0216870609f589f80c6580", null ],
    [ "Save", "class_client_app_1_1_damage_repos.html#a825372a65a35dc3998d1bbc84e5efe71", null ],
    [ "Update", "class_client_app_1_1_damage_repos.html#aab6ad6162c84fa2b3d32b9735199ca6d", null ],
    [ "damageContext", "class_client_app_1_1_damage_repos.html#ae3f7015a7452ffae951386c9fbacc776", null ],
    [ "disposed", "class_client_app_1_1_damage_repos.html#aef69c9077459d185fbaa575196509350", null ]
];