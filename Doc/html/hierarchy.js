var hierarchy =
[
    [ "ClientApp.Damage", "class_client_app_1_1_damage.html", null ],
    [ "DbContext", null, [
      [ "ClientApp.DamageContext", "class_client_app_1_1_damage_context.html", null ],
      [ "ClientApp.Gym_desc_Context", "class_client_app_1_1_gym__desc___context.html", null ],
      [ "ClientApp.Gym_rent_Context", "class_client_app_1_1_gym__rent___context.html", null ],
      [ "ClientApp.GymContext", "class_client_app_1_1_gym_context.html", null ],
      [ "ClientApp.PhotoContext", "class_client_app_1_1_photo_context.html", null ],
      [ "ClientApp.RenterContext", "class_client_app_1_1_renter_context.html", null ],
      [ "ClientApp.StockContext", "class_client_app_1_1_stock_context.html", null ],
      [ "ClientApp.StockRentContext", "class_client_app_1_1_stock_rent_context.html", null ]
    ] ],
    [ "Form", null, [
      [ "ClientApp.Add_renter_form", "class_client_app_1_1_add__renter__form.html", null ],
      [ "ClientApp.Arenda_Form", "class_client_app_1_1_arenda___form.html", null ],
      [ "ClientApp.Edit_Invent_Form", "class_client_app_1_1_edit___invent___form.html", null ],
      [ "ClientApp.Edit_Room_Form", "class_client_app_1_1_edit___room___form.html", null ],
      [ "ClientApp.Form1", "class_client_app_1_1_form1.html", null ],
      [ "ClientApp.Select_rents_form", "class_client_app_1_1_select__rents__form.html", null ]
    ] ],
    [ "ClientApp.Gym", "class_client_app_1_1_gym.html", null ],
    [ "ClientApp.Gym_description", "class_client_app_1_1_gym__description.html", null ],
    [ "ClientApp.Gym_rent", "class_client_app_1_1_gym__rent.html", null ],
    [ "IDisposable", null, [
      [ "ClientApp.DamageRepos", "class_client_app_1_1_damage_repos.html", null ],
      [ "ClientApp.Gym_descRepos", "class_client_app_1_1_gym__desc_repos.html", null ],
      [ "ClientApp.Gym_rentRepos", "class_client_app_1_1_gym__rent_repos.html", null ],
      [ "ClientApp.GymRepos", "class_client_app_1_1_gym_repos.html", null ],
      [ "ClientApp.PhotoRepos", "class_client_app_1_1_photo_repos.html", null ],
      [ "ClientApp.RenterRepos", "class_client_app_1_1_renter_repos.html", null ],
      [ "ClientApp.StockRentRepos", "class_client_app_1_1_stock_rent_repos.html", null ],
      [ "ClientApp.StockRepos", "class_client_app_1_1_stock_repos.html", null ]
    ] ],
    [ "ClientApp.Photo", "class_client_app_1_1_photo.html", null ],
    [ "ClientApp.Renter", "class_client_app_1_1_renter.html", null ],
    [ "ClientApp.Stock", "class_client_app_1_1_stock.html", null ],
    [ "ClientApp.StockRent", "class_client_app_1_1_stock_rent.html", null ]
];