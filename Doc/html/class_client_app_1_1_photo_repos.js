var class_client_app_1_1_photo_repos =
[
    [ "PhotoRepos", "class_client_app_1_1_photo_repos.html#a07a23f7090375bb0c3766da71372177b", null ],
    [ "Create", "class_client_app_1_1_photo_repos.html#acc718e39a035dba9797feca9f254de83", null ],
    [ "Delete", "class_client_app_1_1_photo_repos.html#a124df46a7d17c269b1d22292fb38e932", null ],
    [ "Dispose", "class_client_app_1_1_photo_repos.html#aa317f5accf2afd486a11e2476e89dcee", null ],
    [ "Dispose", "class_client_app_1_1_photo_repos.html#a11583386db1beb4670ddfed602a1a0f4", null ],
    [ "getAll", "class_client_app_1_1_photo_repos.html#a1ad67b159411da7734e9c3e7f34bbd02", null ],
    [ "getById", "class_client_app_1_1_photo_repos.html#a6bcfa80a4ab7795e7252b75c47ccf37b", null ],
    [ "Save", "class_client_app_1_1_photo_repos.html#a505fb5545498b3ef1c324ed767e1bd5e", null ],
    [ "Update", "class_client_app_1_1_photo_repos.html#a8ab31ddba8e017908eb9c0a40196c70e", null ],
    [ "disposed", "class_client_app_1_1_photo_repos.html#a9d1248f8802a2d8f0353c362af33aaea", null ],
    [ "photoContext", "class_client_app_1_1_photo_repos.html#a030d096a2b99f0780589625b70f18a68", null ]
];