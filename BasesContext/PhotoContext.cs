﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClientApp
{
    /// <summary>
    /// Класс для организации доступа к объектам базы данных
    /// </summary>
    class PhotoContext : DbContext
    {
        /// <summary>
        /// Управление объектами
        /// </summary>
        public DbSet<Photo>     Photos      { get; set; }

        public PhotoContext() : base(nameOrConnectionString: "postgresConnection") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            base.OnModelCreating(modelBuilder);
        }
    }
}
