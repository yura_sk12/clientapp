﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClientApp
{
    /// <summary>
    /// Класс для работы с объектом Renter (Арендатор) базы данных
    /// </summary>
    class RenterRepos : IDisposable
    {
        private readonly RenterContext renterContext;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public RenterRepos()
        {
            this.renterContext = new RenterContext();
        }

        /// <summary>
        /// Метод получения всех объектов БД
        /// </summary>
        /// <returns>Список объектов</returns>
        public IEnumerable<Renter> getAll()
        {
            return this.renterContext.Renters;
        }


        /// <summary>
        /// Получение объекта по id
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Объект Renter</returns>
        public Renter getById(long id)
        {
            return this.renterContext.Renters.Find(id);
        }

        /// <summary>
        /// Создание нового объекта в БД
        /// </summary>
        /// <param name="renter">Объект Renter</param>
        public void Create(Renter renter)
        {
            renterContext.Renters.Add(renter);
        }

        /// <summary>
        /// Обновление объекта в БД
        /// </summary>
        /// <param name="renter">Объект Renter</param>
        public void Update(Renter renter)
        {
            renterContext.Entry(renter).State = EntityState.Modified;
        }

        /// <summary>
        /// Удаление объекта из БД
        /// </summary>
        /// <param name="id">Идентификатор Renter</param>
        public void Delete(long id)
        {
            Renter renter = renterContext.Renters.Find(id);
            if (renter != null)
                renterContext.Renters.Remove(renter);
        }

        /// <summary>
        /// Созранение изменений
        /// </summary>
        public void Save()
        {
            renterContext.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.renterContext.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
