﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace ClientApp
{
    /// <summary>
    /// Класс для работы с объектом Gym (Аренда помещения) базы данных
    /// </summary>
    class GymRepos : IDisposable
    {
        private readonly GymContext gymContext;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public GymRepos()
        {
            this.gymContext = new GymContext();
        }

        /// <summary>
        /// Метод получения всех объектов БД
        /// </summary>
        /// <returns>Список объектов</returns>
        public IEnumerable<Gym> getAll()
        {
            return this.gymContext.Gyms;
        }

        /// <summary>
        /// Получение объекта по id
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Объект Gym</returns>
        public Gym getById(long id)
        {
            return gymContext.Gyms.Find(id);
        }

        /// <summary>
        /// Создание нового объекта в БД
        /// </summary>
        /// <param name="gym_in">Объект Gym</param>
        public void Create(Gym gym_in)
        {
            gymContext.Gyms.Add(gym_in);
        }

        /// <summary>
        /// Обновление объекта в БД
        /// </summary>
        /// <param name="gym_in">Объект Gym</param>
        public void Update(Gym gym_in)
        {
            gymContext.Entry(gym_in).State = EntityState.Modified;
        }

        /// <summary>
        /// Удаление объекта из БД
        /// </summary>
        /// <param name="id">Идентификатор Gym</param>
        public void Delete(long id)
        {
            Gym gym = gymContext.Gyms.Find(id);
            if (gym != null)
                gymContext.Gyms.Remove(gym);
        }

        /// <summary>
        /// Сохранение изменений объектов в БД
        /// </summary>
        public void Save()
        {
            gymContext.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    gymContext.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
