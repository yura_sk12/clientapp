﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClientApp
{
    /// <summary>
    /// Класс для работы с объектом Gym_rent (Аренда помещения) базы данных
    /// </summary>
    class Gym_rentRepos : IDisposable
    {
        private readonly Gym_rent_Context gym_rent_Context;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public Gym_rentRepos()
        {
            this.gym_rent_Context = new Gym_rent_Context();
        }

        /// <summary>
        /// Метод получения всех объектов БД
        /// </summary>
        /// <returns>Список объектов Gym_rent</returns>
        public IEnumerable<Gym_rent> getAll()
        {
            return this.gym_rent_Context.Gym_Rents;
        }

        /// <summary>
        /// Получение объекта Gym_rent
        /// </summary>
        /// <param name="gym_id">Идентификатор Gym</param>
        /// <param name="renter_id">Идентификатор Renter</param>
        /// <returns>Объект Gym_rent</returns>
        public Gym_rent getById(long gym_id, long renter_id)
        {
            return gym_rent_Context.Gym_Rents.Find(gym_id, renter_id);
        }

        /// <summary>
        /// Создание нового объекта в БД
        /// </summary>
        /// <param name="gym_rent_in">Объект Gym_rent</param>
        public void Create(Gym_rent gym_rent_in)
        {
            gym_rent_Context.Gym_Rents.Add(gym_rent_in);
        }

        /// <summary>
        /// Обновление объекта Gym_rent в БД
        /// </summary>
        /// <param name="gym_rent_in">Объект Gym_rent</param>
        public void Update(Gym_rent gym_rent_in)
        {
            gym_rent_Context.Entry(gym_rent_in).State = EntityState.Modified;
        }

        /// <summary>
        /// Удаление объекта Gym_rent из БД
        /// </summary>
        /// <param name="gym_rent">Объект Gym_rent</param>
        public void Delete(Gym_rent gym_rent)
        {
            gym_rent_Context.Gym_Rents.Remove(gym_rent);
        }

        /// <summary>
        /// Сохранение изменений объектов в БД
        /// </summary>
        public void Save()
        {
            gym_rent_Context.SaveChanges();
        }

        /// <summary>
        /// Поиск всех GymRent по gym_id
        /// </summary>
        /// <param name="id">Идентификатор Gym</param>
        /// <returns>Список объектов Gym_rent</returns>
        internal IEnumerable<Gym_rent> getByIdGym(long id)
        {
            return this.gym_rent_Context.Gym_Rents.Where(a => a.gym_id == id);
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    gym_rent_Context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
