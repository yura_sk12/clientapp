﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClientApp
{
    /// <summary>
    /// Класс для работы с объектом Gym_description (Описание помещения) базы данных
    /// </summary>
    class Gym_descRepos : IDisposable
    {
        private readonly Gym_desc_Context gym_Desc_Context;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public Gym_descRepos()
        {
            this.gym_Desc_Context = new Gym_desc_Context();
        }

        /// <summary>
        /// Метод получения всех объектов БД
        /// </summary>
        /// <returns>Список объектов Gym_description</returns>
        public IEnumerable<Gym_description> getAll()
        {
            return this.gym_Desc_Context.Gym_Descriptions;
        }

        /// <summary>
        /// Получение описания помещения по id
        /// </summary>
        /// <param name="id">Идентификатор Gym_description</param>
        /// <returns>Объект Gym_description</returns>
        public Gym_description getById(long id)
        {
            return gym_Desc_Context.Gym_Descriptions.Find(id);
        }

        /// <summary>
        /// Поиск всех Gym_description по идентификатору Gym
        /// </summary>
        /// <param name="id">Идентификатор Gym</param>
        /// <returns>Список объектов Gym_description</returns>
        public IEnumerable<Gym_description> getByGymID(long id)
        {
            return gym_Desc_Context.Gym_Descriptions.Where(a => a.id_gym == id);
        }

        /// <summary>
        /// Создание нового объекта Gym_description в БД
        /// </summary>
        /// <param name="gym_description_in">Объект Gym_description</param>
        public void Create(Gym_description gym_description_in)
        {
            gym_Desc_Context.Gym_Descriptions.Add(gym_description_in);
        }

        /// <summary>
        /// Обновление объекта Gym_description в БД
        /// </summary>
        /// <param name="gym_description_in">Объект Gym_description</param>
        public void Update(Gym_description gym_description_in)
        {
            gym_Desc_Context.Entry(gym_description_in).State = EntityState.Modified;
        }

        /// <summary>
        /// Удаление объекта Gym_description по его id из БД
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            Gym_description gym_description = gym_Desc_Context.Gym_Descriptions.Find(id);
            if (gym_description != null)
                gym_Desc_Context.Gym_Descriptions.Remove(gym_description);
        }

        /// <summary>
        /// Сохранение изменений объектов в БД
        /// </summary>
        public void Save()
        {
            gym_Desc_Context.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    gym_Desc_Context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
