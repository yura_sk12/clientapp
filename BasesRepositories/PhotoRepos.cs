﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace ClientApp
{
    /// <summary>
    /// Класс для работы с объектом Photo (Изображение) базы данных
    /// </summary>
    class PhotoRepos : IDisposable
    {
        private readonly PhotoContext photoContext;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public PhotoRepos()
        {
            this.photoContext = new PhotoContext();
        }

        /// <summary>
        /// Метод получения всех объектов БД
        /// </summary>
        /// <returns>Список объектов</returns>
        public IEnumerable<Photo> getAll()
        {
            return this.photoContext.Photos;
        }

        /// <summary>
        /// Получение объекта по id
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Объект Photo</returns>
        public Photo getById(long id)
        {
            return photoContext.Photos.Find(id);
        }

        /// <summary>
        /// Создание нового объекта в БД
        /// </summary>
        /// <param name="photo_in">Объект Photo</param>
        public void Create(Photo photo_in)
        {
            photoContext.Photos.Add(photo_in);
        }

        /// <summary>
        /// Обновление объекта в БД
        /// </summary>
        /// <param name="photo_in">Объект Photo</param>
        public void Update(Photo photo_in)
        {
            photoContext.Entry(photo_in).State = EntityState.Modified;
        }

        /// <summary>
        /// Удаление объекта из БД
        /// </summary>
        /// <param name="id">Идентификатор Photo</param>
        public void Delete(long id)
        {
            Photo photo = photoContext.Photos.Find(id);
            if (photo != null)
                photoContext.Photos.Remove(photo);
        }

        /// <summary>
        /// Сохранение изменений объектов в БД
        /// </summary>
        public void Save()
        {
            photoContext.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    photoContext.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
