﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClientApp
{
    /// <summary>
    /// Класс для работы с объектом StockRent (Аренда инвентаря) базы данных
    /// </summary>
    class StockRentRepos : IDisposable
    {
        private readonly StockRentContext stockRentContext;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public StockRentRepos()
        {
            this.stockRentContext = new StockRentContext();
        }

        /// <summary>
        /// Метод получения всех объектов БД
        /// </summary>
        /// <returns>Список объектов StockRent</returns>
        public IEnumerable<StockRent> getAll()
        {
            return this.stockRentContext.StockRents;
        }

        /// <summary>
        /// Получение объекта StockRent
        /// </summary>
        /// <param name="id_stock">Идентификатор Stock</param>
        /// <param name="id_renter">Идентификатор Renter</param>
        /// <returns>Объект StockRent</returns>
        public StockRent getById(long id_stock, long id_renter)
        {
            return this.stockRentContext.StockRents.Find(id_stock, id_renter);
        }

        /// <summary>
        /// Поиск всех StockRent по идентификатору Stock
        /// </summary>
        /// <param name="id">Идентификатор Stock</param>
        /// <returns>Список объектов StockRent</returns>
        internal IEnumerable<StockRent> getByIdStock(long id)
        {
            return this.stockRentContext.StockRents.Where(a => a.stock_id == id);
        }

        /// <summary>
        /// Создание нового объекта в БД
        /// </summary>
        /// <param name="stockRent">Объект StockRent</param>
        public void Create(StockRent stockRent)
        {
            stockRentContext.StockRents.Add(stockRent);
        }

        /// <summary>
        /// Обновление объекта в БД
        /// </summary>
        /// <param name="stockRent">Объект StockRent</param>
        public void Update(StockRent stockRent)
        {
            stockRentContext.Entry(stockRent).State = EntityState.Modified;
        }

        /// <summary>
        /// Удаление объекта из БД
        /// </summary>
        /// <param name="stockrent">Объект StockRent</param>
        public void Delete(StockRent stockrent)
        {
           stockRentContext.StockRents.Remove(stockrent);
        }

        /// <summary>
        /// Созранение изменений
        /// </summary>
        public void Save()
        {
            stockRentContext.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.stockRentContext.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
