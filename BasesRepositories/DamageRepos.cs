﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClientApp
{
    /// <summary>
    /// Класс для работы с объектом Damage базы данных
    /// </summary>
    class DamageRepos : IDisposable
    {
        private readonly DamageContext damageContext;
        
        /// <summary>
        /// Конструктор класса
        /// </summary>
        public DamageRepos()
        {
            this.damageContext = new DamageContext();
        }


        /// <summary>
        /// Метод получения всех объектов БД
        /// </summary>
        /// <returns>Список объектов Damage</returns>
        public IEnumerable<Damage> getAll()
        {
            return this.damageContext.Damages;
        }

        /// <summary>
        /// Получение объекта по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Объект Damage</returns>
        public Damage getById(long id)
        {
            return this.damageContext.Damages.Find(id);
        }

        /// <summary>
        /// Поиск всех Damage по идентификатору Stock
        /// </summary>
        /// <param name="id">Идентификатор Stock</param>
        /// <returns>Список объектов Damage</returns>
        public IEnumerable<Damage> getByStockID(long id)
        {
            return this.damageContext.Damages.Where(a => a.stock_id == id);
        }

        /// <summary>
        /// Создание нового объекта Damage в БД
        /// </summary>
        /// <param name="damage">Объект Damage</param>
        public void Create(Damage damage)
        {
            damageContext.Damages.Add(damage);
        }

        /// <summary>
        /// Обновление объекта Damage в БД
        /// </summary>
        /// <param name="damage">Объект Damage</param>
        public void Update(Damage damage)
        {
            damageContext.Entry(damage).State = EntityState.Modified;
        }

        /// <summary>
        /// Удаление объекта Damage по его id из БД
        /// </summary>
        /// <param name="id">id Damage</param>
        public void Delete(long id)
        {
            Damage damage = damageContext.Damages.Find(id);
            if (damage != null)
                damageContext.Damages.Remove(damage);
        }

        /// <summary>
        /// Сохранение изменений объектов Damage в БД
        /// </summary>
        public void Save()
        {
            damageContext.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.damageContext.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
