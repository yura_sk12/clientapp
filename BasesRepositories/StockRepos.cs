﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClientApp
{
    /// <summary>
    /// Класс для работы с объектом Stock (Инвентарь) базы данных
    /// </summary>
    class StockRepos : IDisposable
    {
        private readonly StockContext stockContext;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public StockRepos()
        {
            this.stockContext = new StockContext();
        }

        /// <summary>
        /// Метод получения всех объектов БД
        /// </summary>
        /// <returns>Список объектов</returns>
        public IEnumerable<Stock> getAllStock()
        {
            return this.stockContext.Stocks;
        }

        /// <summary>
        /// Получение объекта по id
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Объект Stock</returns>
        public Stock getById(long id)
        {
            return stockContext.Stocks.Find(id);
        }

        /// <summary>
        /// Создание нового объекта в БД
        /// </summary>
        /// <param name="stock">Объект Stock</param>
        public void Create(Stock stock)
        {
            stockContext.Stocks.Add(stock);
        }

        /// <summary>
        /// Обновление объекта в БД
        /// </summary>
        /// <param name="stock">Объект Stock</param>
        public void Update(Stock stock)
        {
            stockContext.Entry(stock).State = EntityState.Modified;
        }

        /// <summary>
        /// Удаление объекта из БД
        /// </summary>
        /// <param name="id">Идентификатор Stock</param>
        public void Delete(long id)
        {
            Stock stock = stockContext.Stocks.Find(id);
            if (stock != null)
                stockContext.Stocks.Remove(stock);
        }

        /// <summary>
        /// Сохранение изменений объектов в БД
        /// </summary>
        public void Save()
        {
            stockContext.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.stockContext.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
